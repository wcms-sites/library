core = 7.x
api = 2

; faq
projects[faq][type] = "module"
projects[faq][download][type] = "git"
projects[faq][download][url] = "https://git.uwaterloo.ca/drupal-org/faq.git"
projects[faq][download][tag] = "7.x-1.0"
projects[faq][download][subdir] = ""

; uw_lib_ask_us
projects[uw_lib_ask_us][type] = "module"
projects[uw_lib_ask_us][download][type] = "git"
projects[uw_lib_ask_us][download][url] = "https://git.uwaterloo.ca/library/uw_lib_ask_us.git"
projects[uw_lib_ask_us][download][tag] = "7.x-2.1"
projects[uw_lib_ask_us][subdir] = ""

; uw_cfg_redirect
projects[uw_cfg_redirect][type] = "module"
projects[uw_cfg_redirect][download][type] = "git"
projects[uw_cfg_redirect][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_redirect.git"
projects[uw_cfg_redirect][download][tag] = "7.x-1.9-uw_lib1"
projects[uw_cfg_redirect][subdir] = ""


